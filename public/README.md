# Taiwanese Mandarin resources

## Textbooks
- A Course in Contemporary Chinese ([Official](http://web.mtc.ntnu.edu.tw/eng/book/A_Course_in_Contemporary_Chinese.html)) ([Amazon](https://www.amazon.com/Course-Contemporary-Chinese-Textbook/dp/9570845694/))
- [Practical Audio-Visual Chinese](https://www.amazon.com/Practical-Audio-Visual-Chinese-2nd-Book/dp/957091789X)
- [Modern Chinese](https://www.books.com.tw/products/0010823196)

## Electronic Textbooks
- [Huayu textbooks](https://www.huayuworld.org/material-download.php?volume=12)
- [Speak Mandarin in 500 Words](https://www.huayuworld.org/eBookstore/PDF/%E5%9C%8B%E5%88%A5%E5%8C%96%E6%95%99%E6%9D%90/001.%E4%BA%94%E7%99%BE%E5%AD%97%E8%AA%AA%E8%8F%AF%E8%AA%9E%E4%B8%AD%E8%8B%B1%E6%96%87%E7%89%88.pdf)
  - [Chinese audio](https://www.huayuworld.org/eBookstore/media/五百字說華語課文音檔/五百字說華語課文音檔.zip)
  - [English explanations](https://www.huayuworld.org/eBookstore/media/500字說華語中英版音檔-20180830.zip)

## Dictionaries
- Pleco phone app ([Android](https://play.google.com/store/apps/details?id=com.pleco.chinesesystem)) ([iPhone](https://itunes.apple.com/us/app/pleco-chinese-dictionary/id341922306?mt=8&uo=4&at=11l4Wi)) (Use free Cross-strait and Moedict add-on dictionaries)
- [Cross-strait dictionary](https://www.moedict.tw/~%E8%90%8C)
- [Dr. Eye](https://yun.dreye.com/dict_new/dict.php)
- [教育百科](https://pedia.cloud.edu.tw/)
- [Moedict](https://www.moedict.tw/)
- [Outlier (paid)](https://www.outlier-linguistics.com/pages/chinese-demo)

## Bopomofo / Zhuyin Fuhao (注音符號)
- [Learn Bopomofo in 15 minutes](https://www.youtube.com/watch?v=AKH5IHhbUUA)
- [Stroke Order & Pronunciation](https://stroke-order.learningweb.moe.edu.tw/symbolStrokeQuery.do?lang=en)
- [Pronunciation](https://www.mdnkids.com/BoPoMo/)
- [Keyboard layout](https://noctivagous.com/bopomofo/)

## Fonts
- [MoE Kai](https://language.moe.gov.tw/001/Upload/Files/site_content/M0001/edukai-4.0.zip)
- [cns11643 Fonts](https://www.cns11643.gov.tw/AIDB/Open_Data.zip)
- [Wang fonts](https://code.google.com/archive/p/wangfonts/downloads)

## Reading Materials
- [HyRead ebooks](https://huayuworld.ebook.hyread.com.tw/searchList.jsp?scope=2&search_field=BSU&search_input=all&&filter=1)
- [Huayu Biweekly](https://biweekly.huayuworld.org/)

## Video Materials
- [Huayu Teaching Videos](https://www.huayuworld.org/teaching-videos.php)
- [Youglish](https://youglish.com/chinese)

## Audio Materials
- [Learn Taiwanese Mandarin](https://lear-taiwanese-mandarin.webnode.tw/)
- [還可中文](https://haikemandarintw.blogspot.com/)

## Shows
- [Sugoideas](https://sugoideas.com/)

### Variety shows

#### 2分之一強
Foreigners who have learned Mandarin and live in Taiwan:

- <https://www.youtube.com/watch?v=i5bDBhKASRo>

#### 天才衝衝衝
Game show with a lot of different games:

- <https://www.youtube.com/watch?v=0CxGw-VndI4>
- <https://www.youtube.com/watch?v=H_AzZN0vwHk>

#### 康熙來了
An interview show

- <https://www.youtube.com/watch?v=25tNknabaeQ>

#### 阿滴
He teaches Taiwanese people English and usually speaks in Mandarin

- <https://www.youtube.com/watch?v=6mklxNlzNhI>

#### Anything with Jackie Wu
He has a lot of game shows and talk shows

- <https://www.youtube.com/watch?v=2o9ae3xM4Pg>

## Taiwanese Gaming Youtubers
- [阿薩](https://www.youtube.com/c/AzaGames)
- [聶寶](https://www.youtube.com/user/nielnieh510)
- [舞秋風](https://www.youtube.com/user/MrChesterccj)
- [喵哈](https://www.youtube.com/c/sunflower657s)
- [巧克力](https://www.youtube.com/c/oeurxhichocolate)
- [老皮](https://www.youtube.com/user/tolocat)
- [阿神](https://www.youtube.com/c/charlie615119)
- [阿謙](https://www.youtube.com/user/oeurstudiolive)
- [鬼鬼](https://www.youtube.com/c/Onityan)
- [阿洛](https://www.youtube.com/user/losn555412)
- [沙皮塞](https://www.youtube.com/c/Xavier%E5%A1%9E%E7%B6%AD%E7%88%BE%E5%AF%A6%E6%B3%81%E9%A0%BB%E9%81%93Successor)

## Miscellaneous

### TOCFL Vocabulary Lists
- [Books](https://tocfl.edu.tw/index.php/exam/materials/list/1)
- [Vocab lists](https://tocfl.edu.tw/index.php/exam/download)
- [Mock tests](https://tocfl.edu.tw/index.php/exam/test/page/2)

### Offical Standards
- [Punctuation](https://language.moe.gov.tw/001/upload/files/site_content/m0001/hau/h12.htm)
